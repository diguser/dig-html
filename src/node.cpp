#include "dig-html.hpp"
#include <algorithm>
#include <iostream>

namespace DIG {
namespace HTML {

Node::Node(){
  type = OBJECT;
}

Node::Node(const std::string s){
  tag_name = s;
  std::transform(tag_name.begin(), tag_name.end(), tag_name.begin(), ::tolower);
  type = OBJECT;
}

void Node::addParam(const std::string p,const std::string v){
  {
  Param t;
  params.push_back(t);
  }
  params[params.size()-1].param = p.substr();
  std::transform(params[params.size()-1].param.begin(), params[params.size()-1].param.end(), params[params.size()-1].param.begin(), ::tolower);
  params[params.size()-1].value = v.substr();
  //std::cout << "  Param: " << t.param << " = '" << t.value << "'" << std::endl;
  
}

unsigned Node::addNode(const Node n){
  if(type != OBJECT)
    return 0;
  content.push_back(n);
  return content.size()-1;
}

void Node::setString(const std::string s){
  type = STRING;
  content_str = s;
}
void Node::setTagName(const std::string s){
  tag_name = s;
  std::transform(tag_name.begin(), tag_name.end(), tag_name.begin(), ::tolower);
}

Node& Node::getNode(const unsigned i,uint8_t* p){
  if(type==STRING){
    if(p!=nullptr)
      *p=-2;
    return *this;
  }
  if(i>=content.size()){
    if(p!=nullptr)
      *p=-1;
    return *this;
  }
  if(p!=nullptr)
    *p=0;
  return content[i];
}

Node& Node::getNode(const std::string s,const unsigned i,uint8_t* p){
  if(type==STRING){
    if(p!=nullptr)
      *p=-2;
    return *this;
  }
  unsigned count = 0;
  for(unsigned j=0;j<content.size();j++){
    if(content[j].getTagName().compare(s)==0){
      if(count == i){
        if(p!=nullptr)
          *p=0;
        return content[j];
      }
      count++;
    }
  }
  if(p!=nullptr)
    *p=-1;
  return *this;
}

std::string Node::to_string(){
  if(type==STRING)
    return content_str;
  std::string v = "<";
  v+=tag_name;
  for(std::vector<Param>::iterator it=params.begin();it!=params.end();it++){
    v+=" ";
    v+=it->param + "=\"";
    v+=it->value + "\"";
    
    // std::cout << "     Addresses: " << &(it->param) << " " << &(it->value) << std::endl;
  }
  if(!hasEndTag(tag_name))
    return v+"/>";
  v+=">";
  for(std::vector<Node>::iterator it=content.begin();it!=content.end();it++){
    v+=it->to_string();
  }
  return v+std::string("</")+tag_name+std::string(">");
}

std::string Node::getParam(const std::string s){
  for(std::vector<Param>::iterator it=params.begin();it!=params.end();it++){
    if(it->param.compare(s)==0)
      return it->value;
  }
  return "";
}

std::string Node::getTagName(){
  return tag_name;
}

unsigned Node::size(){
  if(type==OBJECT)
    return content.size();
  return content_str.length();
}

unsigned Node::getType(){
  return type;
}

void Node::clear(){
  type = OBJECT;
  params.clear();
  content.clear();
  tag_name.clear();
  content_str.clear();
}

bool Node::hasEndTag(const std::string tag){
  std::string data=tag;
  std::transform(data.begin(), data.end(), data.begin(), ::tolower);
  if((data.compare("img")==0)||(data.compare("br")==0)||
     (data.compare("hr")==0)||(data.compare("link")==0)||
     (data.compare("base")==0)||(data.compare("meta")==0)||
     (data.compare("input")==0))
    return false;
  return true;
}

}}