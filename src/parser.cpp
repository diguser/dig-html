#include "dig-html.hpp"

#include <stack>
#include <algorithm>
#include <iostream>

namespace DIG {
namespace HTML {

int param_decode(Node& t,size_t& p01, size_t& p02, const std::string data){
  char sep;
  uint8_t mode = 0;
  std::string p_name,p_value;
  for(unsigned i=p01;i<p02;i++){
    if(mode == 0){
      if((data[i] == ' ')||(data[i] == '/')){
        if(p_name.length() == 0)
          continue;
        t.addParam(p_name,"1");
        p_name = "";
        p_value = "";
        continue;
      }
      if(data[i] == '='){
        mode = 1;
        continue;
      }
      p_name+=data[i];
    }else if(mode == 1){
      if((data[i] == '"')||(data[i] == '\'')){
        sep = data[i];
        mode = 2;
        continue;
      }
      p_value+=data[i];
      mode = 3;
    }else if(mode == 2){
      if((data[i] == sep)&&(data[i-1] != '\\')&&(data[i-2] != '\\')){
        t.addParam(p_name,p_value);
        mode = 0;
        p_name = "";
        p_value = "";
        continue;
      }
      p_value += data[i];
    }else if(mode == 3){
      if((data[i] == ' ')||(data[i] == '/')){
        t.addParam(p_name,p_value);
        mode = 0;
        p_name = "";
        p_value = "";
        continue;
      }
      p_value+=data[i];
    }
  }
  return 0;
}

int parse(Node& parent,const std::string data){
  typedef struct{size_t start,end;} P;
  P p;
  size_t p01,p02,p03;
  parent.clear();
  Node* actual = &parent;
  std::stack<Node*> Nodes;
  std::string tag_name;
  p.start = 0;
  p.end = data.length();
  unsigned pp = 0;
  
  #define RET \
  if(Nodes.size()==0)\
    break;\
  actual = Nodes.top();\
  Nodes.pop();\
  continue;
  
  do{
    if(p.start>=data.length()){
      if(Nodes.size()==0)
        break;
      actual = Nodes.top();
      Nodes.pop();
      // std::cout << "Up one" << std::endl;
      continue;
    }
    
    p03 = data.find("<",p.start);
    if((p03 == std::string::npos)||(p03>=p.end)){
      // Não há tag nesse intervalo
      //if(actual->size()!=0){
        Node t;
        t.setString(data.substr(p.start,p.end-p.start));
        actual->addNode(t);
      //}else{
      //  actual->setString(data.substr(p.start,p.end-p.start));
      //}
      p.start = p.end;
      continue;
    }
    
    if(p03 != p.start){
      // Há texto/espaço antes da primeira tag encontrada
      Node t;
      t.setString(data.substr(p.start,p03-p.start));
      actual->addNode(t);
      p.start=p03;
    }
    
    
    p01 = data.find(" ",p.start);
    p02 = data.find(">",p.start);
    if(p01 == std::string::npos)
      p01=p02;
    else if(p02<p01)
      p01 = p02;
    
    if(p01==std::string::npos){
      //std::cout << "Unexpected final '>' not found." << std::endl;
      return -1;
    }
    if(p02==std::string::npos){
      //std::cout << "Unexpected final '>' not found 2." << std::endl;
      return -2;
    }
    
    if(data[p.start+1] == '/'){
      
      // This is a final tag.
      tag_name = data.substr(p.start+2,p01-p.start-2);
      std::transform(tag_name.begin(), tag_name.end(), tag_name.begin(), ::tolower);
      
      
      while(actual->getTagName().compare(tag_name) != 0){
        // std::cout << " [] " << actual->getTagName() << "  <>  " << tag_name << std::endl;
        if(Nodes.size()==0){
          return -5;
        }
        actual=Nodes.top();
        Nodes.pop();
      }
      if(Nodes.size()==0){
        return -6;
      }
      actual = Nodes.top();
      Nodes.pop();
      p.start = p02+1;
      continue;
      
    }
    
    if(data[p.start+1] == '!'){
      // É um comentário..
      if(data[p.start+2] == '-')
        p02 = data.find("-->",p.start)+2;
      p.start = p02+1;
      continue;
    }else{
      Node t;
      pp = actual->addNode(t);
      (actual->getNode(pp)).setTagName(data.substr(p.start+1,p01-p.start-1));
    }
    
    // std::cout << "New node: " << (actual->getNode(pp)).getTagName() << std::endl;
    
    if(p02>p01){
      // Tem parametros
      param_decode((actual->getNode(pp)),p01,p02,data);
      /*
      if((actual->getNode(pp)).getTagName().compare("img") == 0)
        std::cout << " Formated: " << (actual->getNode(pp)).to_string() << std::endl;
      */
    }
    
    if(Node::hasEndTag((actual->getNode(pp)).getTagName())){
      
      
      if((actual->getNode(pp)).getTagName().compare("script")==0){
        Node z;
        p03 = data.find("</script>",p02);
        z.setString(data.substr(p02,p03-p02));
        (actual->getNode(pp)).addNode(z);
        p.start = p03+9;
        continue;
      }
      
      Nodes.push(actual);
      actual = &((actual->getNode(pp)));
      p.start = p02+1;
      
      
      continue;
      
    }else{
      // Se não possuir tag finalizadora
      p.start = p02+1;
      continue;
    }
    
  }while(true);
  
  return 0;
}

}}