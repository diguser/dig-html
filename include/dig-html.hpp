#ifndef _DIG_HTML_HPP_
#define _DIG_HTML_HPP_ 1

#include <string>
#include <vector>

namespace DIG {
namespace HTML {

typedef struct {std::string param,value;} Param;

/**
 * Example:
 * <div><span>huehuebr</span></div>
 *
 * Node structure:
 * Node("div")        {type: OBJECT}
 *  |
 *  +-> Node("span")  {type: OBJECT}
 *       |
 *       +-> Node()   {type: STRING; content: huehuebr}
 */

class Node {
  private:
    std::string tag_name;
    std::vector<Param> params;
    std::vector<Node> content;
    std::string content_str;
    uint8_t type;
  public:
    Node();
    // Construct object and set the tag name
    Node(const std::string);
    
    void addParam(const std::string,const std::string);
    unsigned addNode(const Node);
    void setString(const std::string);
    void setTagName(const std::string);
    void clear();
    
    /**
     * getNode() functions always return the requested node or itself when fail;
     *
     * The pointer is used as error return.
     * 0  = Successful
     * -1 = node not found
     * -2 = is is string type
     * -3 = has error
     */
    Node&       getNode(const unsigned=0,uint8_t* =nullptr);
    Node&       getNode(const std::string,const unsigned=0,uint8_t* =nullptr);
    
    /**
     * If Node is string type, only returns the string content.
     * Else return html formatted content with all inner Nodes.
     */
    std::string to_string();
    
    /**
     * If fail, return empty string
     */
    std::string getParam(const std::string);
    
    /**
     * Always return tag name.
     */
    std::string getTagName();
    
    /**
     * Return how many Nodes has in that Node.
     */
    unsigned    size();
    
    /**
     * Return the type of Node
     */
    unsigned    getType();
    
    /**
     * Check if that tag name requires end tag
     */
    static bool hasEndTag(const std::string);
    
    enum {
      OBJECT,
      STRING
    };
};

/**
 * Parse a html string into Node's structure
 *
 * Arguments:
 * 1st - Root Node for all content parsed
 * 2nd - String with html content
 *
 * Return:
 * 0     = Successful
 * other = has error
 */
int parse(Node&,const std::string);

}}

#endif