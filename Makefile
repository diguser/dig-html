CC = g++
LD = g++

DIR_SRC = src
DIR_OBJ = obj
DIR_BIN = bin
DIR_LIB = lib
DIR_HEADER = hdr
DIR_RES = res

FILE_DIV =/
ifeq ($(OS),Windows_NT)
  WIN=1
  RM =del /s /f /q
  COLOR_BLUE =
  COLOR_GREEN=
  COLOR_RED  =
  COLOR_DEF  =
else
  WIN=0
  RM =rm -rf
  COLOR_BLUE =\033[1;34m
  COLOR_GREEN=\033[1;32m
  COLOR_RED  =\033[31m
  COLOR_DEF  =\033[0m
endif


CCFLAGS =-Wall -std=gnu++11
LDFLAGS =-Wall

HEADERS = $(wildcard $(DIR_HEADER)$(FILE_DIV)*.hpp)
DIRS = $(DIR_OBJ) $(DIR_LIB)


EX1_SRC =\
node.cpp \
parser.cpp 

EX1_OUT = libdightml.a
EX1_SRC_WPATH =$(addprefix $(DIR_SRC)$(FILE_DIV),$(EX1_SRC))
EX1_OBJ_WPATH =$(addprefix $(DIR_OBJ)$(FILE_DIV),$(patsubst %.cpp,%.o,$(EX1_SRC)))
EX1_OUT_WPATH =$(DIR_LIB)$(FILE_DIV)$(EX1_OUT)

all: $(EX1_OUT)

$(EX1_OUT) $(EX1_OUT_WPATH): $(DIRS) $(EX1_OBJ_WPATH)
ifeq ($(WIN),0)
	-@echo "$(COLOR_BLUE)Linking$(COLOR_GREEN)    $(EX1_OUT_WPATH)$(COLOR_RED)"
	-@$(RM) $(EX1_OUT_WPATH)
	-@$(AR) $(LDFLAGS) $(EX1_OBJ_WPATH) $(LDFLAGS) -o $(EX1_OUT_WPATH)
	@if [ -f $(EX1_OUT_WPATH) ]; then echo "\033[1;32m           Successfully\033[0m"; else echo "Linking    ERROR\033[0m"; return 1; fi;
else
	-@echo Library    $(EX1_OUT_WPATH)
	@$(AR) rcs $(EX1_OUT_WPATH) $(EX1_OBJ_WPATH)
	-@echo            Successfully
endif

$(DIRS):
	-@mkdir $@

$(DIR_OBJ)$(FILE_DIV)%.o: $(DIR_SRC)$(FILE_DIV)%.cpp $(HEADERS)
ifeq ($(WIN),0)
	-@echo "$(COLOR_BLUE)Compile$(COLOR_GREEN)    $<$(COLOR_RED)"
	-@$(RM) $@
	-@$(CC) $(CCFLAGS) -c $< -o $@
	@if [ -f $@ ]; then return 0; else echo "Compile    ERROR\033[0m"; return 1; fi;
else
	-@echo Compile    $<
	@$(CC) $(CCFLAGS) -c $< -o $@
endif

color:
	-@echo $(COLOR_DEF)

clean:
ifeq ($(WIN),0)
	-@echo "$(COLOR_BLUE)Cleaning project$(COLOR_RED)"
	-$(RM) $(EX1_OUT_WPATH)
	-$(RM) $(DIR_OBJ)
	-@echo "$(COLOR_DEF)"
else
	-@echo Cleaning project
	-@$(RM) $(subst /,\\,$(EX1_OUT_WPATH))
	-@$(RM) $(subst /,\\,$(DIR_OBJ))
endif
