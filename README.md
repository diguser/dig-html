# DIG HTML #

Simple library to parse html page/code into c++ structures.

Also can be used to create html page/code using one simple structure.

### Compile ###

Only use:
```bash
$ make
```
*Note:* Tested in Windows with MingW.

### Node Class ###
That unique class can assume two types: *String* or *Object*

As string, the Node will ignore tag name, parameters and not accept internal nodes.

All string in html will be parsed as one Node type string.

Ex.:
```html
<div><span>huehuebr</span></div>
```
Has the same parse result as:
```cpp
DIG::HTML::Node n1("div");
DIG::HTML::Node n2("span");
DIG::HTML::Node n3();
n3.setString("huehuebr");
n2.addNode(n3);
n1.addNode(n2);
```

### Contact ###
Want more? Found bug? Tell-me..